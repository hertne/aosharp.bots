﻿using System;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace InfBuddy.IPCMessages
{
    [AoContract((int)IPCOpcode.Faction)]
    public class FactionMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.Faction;

        [AoMember(0)]
        public MissionFaction MissionFaction { get; set; }
    }
}
