﻿using System;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace InfBuddy.IPCMessages
{
    [AoContract((int)IPCOpcode.Difficulty)]
    public class DifficultyMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.Difficulty;

        [AoMember(0)]
        public MissionDifficulty MissionDifficulty { get; set; }
    }
}
