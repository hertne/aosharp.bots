﻿using System;
using System.Collections.Generic;
using System.IO;
using AOSharp.Core;
using Newtonsoft.Json;
using AOSharp.Core.UI;

namespace InfBuddy
{
    public class Config
    {
        public Dictionary<int, CharacterSettings> CharSettings { get; set; }

        protected string _path;

        [JsonIgnore]
        public MissionDifficulty MissionDifficulty => CharSettings != null && CharSettings.ContainsKey(Game.ClientInst) ? CharSettings[Game.ClientInst].MissionDifficulty : MissionDifficulty.Easy;
        [JsonIgnore]
        public MissionFaction MissionFaction => CharSettings != null && CharSettings.ContainsKey(Game.ClientInst) ? CharSettings[Game.ClientInst].MissionFaction : MissionFaction.Neutral;

        [JsonIgnore]
        public bool IsLeech => CharSettings != null && CharSettings.ContainsKey(Game.ClientInst) ? CharSettings[Game.ClientInst].IsLeech : false;
        [JsonIgnore]
        public bool DoubleReward => CharSettings != null && CharSettings.ContainsKey(Game.ClientInst) ? CharSettings[Game.ClientInst].DoubleReward : false;
        [JsonIgnore]
        public bool PathToMob => CharSettings != null && CharSettings.ContainsKey(Game.ClientInst) ? CharSettings[Game.ClientInst].PathToMob : false;
        [JsonIgnore]
        public int IPCChannel => CharSettings != null && CharSettings.ContainsKey(Game.ClientInst) ? CharSettings[Game.ClientInst].IPCChannel : 0;

        public static Config Load(string path)
        {
            Config config;

            try
            {
                config = JsonConvert.DeserializeObject<Config>(File.ReadAllText(path));

                config._path = path;
            }
            catch
            {
                Chat.WriteLine($"No config file found.");
                Chat.WriteLine($"Using default settings");

                config = new Config
                {
                    CharSettings = new Dictionary<int, CharacterSettings>()
                    {
                        { Game.ClientInst, new CharacterSettings() }
                    }
                };

                config._path = path;

                config.Save();
            }

            return config;
        }

        public void Save()
        {
            if(!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp"))
                Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp");

            if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\InfBuddy"))
                Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\InfBuddy");

            if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\InfBuddy\\{Game.ClientInst}"))
                Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\InfBuddy\\{Game.ClientInst}");

                File.WriteAllText(_path, JsonConvert.SerializeObject(this, Formatting.Indented));
        }
    }

    public class CharacterSettings
    {
        public MissionDifficulty MissionDifficulty { get; set; } = MissionDifficulty.Easy;
        public MissionFaction MissionFaction { get; set; } = MissionFaction.Neutral;

        public bool IsLeech { get; set; } = false;

        public bool PathToMob { get; set; } = false;

        public bool DoubleReward { get; set; } = false;

        public int IPCChannel { get; set; } = 0;

        public string GetMissionName()
        {
            switch (MissionDifficulty)
            {
                case MissionDifficulty.Easy:
                    if (MissionFaction == MissionFaction.Neutral)
                        return "The Purification Ritual - Easy";
                    else
                        return "The Purification Ritual - Ea...";
                case MissionDifficulty.Medium:
                    return "The Purification Ritual - Me...";
                case MissionDifficulty.Hard:
                    return "The Purification Ritual - Ha...";
                default:
                    return "Unknown";
            }
        }
    }

    [Serializable]

    public enum MissionDifficulty
    {
        Easy = 1,
        Medium = 2,
        Hard = 3
    }

    public enum MissionFaction
    {
        Neutral = 1,
        Omni = 2,
        Clan = 3
    }
}
