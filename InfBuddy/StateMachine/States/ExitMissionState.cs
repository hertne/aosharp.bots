﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System.Threading.Tasks;

namespace InfBuddy
{
    public class ExitMissionState : IState
    {
        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == Constants.OmniPandeGId)
            {
                InfBuddy.NavMeshMovementController.Halt();
                return new DiedState();
            }

            if (Playfield.ModelIdentity.Instance == Constants.ClanPandeGId)
            {
                InfBuddy.NavMeshMovementController.Halt();
                return new DiedState();
            }

            if (Playfield.ModelIdentity.Instance == Constants.InfernoId)
            {
                if (!InfBuddy.Config.DoubleReward)
                {
                    return new ReformState();
                }
                if (InfBuddy.Config.DoubleReward)
                {
                    if (InfBuddy.Trigger == true)
                    {
                        return new MoveToQuestGiverState();
                    }
                    else
                    {
                        return new ReformState();
                    }
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("ExitMissionState::OnStateEnter");

            Task.Factory.StartNew(
                async () =>
                {
                    await Task.Delay(17000);

                    if (!Team.IsLeader && InfBuddy.Config.IsLeech)
                        DynelManager.LocalPlayer.Position = Constants.LeechMissionExit;
                    else
                        InfBuddy.NavMeshMovementController.SetNavMeshDestination(Constants.ExitPos);

                    InfBuddy.NavMeshMovementController.AppendDestination(Constants.ExitFinalPos);
                });
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("ExitMissionState::OnStateExit");

            //if (InfBuddy._research["Toggle"].AsBool())
            //{
            //    if (InfBuddy._research["Line1"].AsBool() && _line1 == false)
            //    {
            //        if (_line8)
            //        {
            //            DynelManager.LocalPlayer.SetStat(Stat.PersonalResearchGoal,
            //                InfBuddy.GetHashLine1(Perk.GetPerkLineLevel(InfBuddy._researchLine1[DynelManager.LocalPlayer.Profession])));

            //            _line8 = false;
            //            _line1 = true;
            //            return;
            //        }
            //        else
            //        {

            //            DynelManager.LocalPlayer.SetStat(Stat.PersonalResearchGoal,
            //                InfBuddy.GetHashLine1(Perk.GetPerkLineLevel(InfBuddy._researchLine1[DynelManager.LocalPlayer.Profession])));

            //            _line1 = true;
            //            return;
            //        }
            //    }
            //    if (InfBuddy._research["Line2"].AsBool() && _line2 == false)
            //    {
            //        if (_line1)
            //        {
            //            DynelManager.LocalPlayer.SetStat(Stat.PersonalResearchGoal,
            //                InfBuddy.GetHashLine2(Perk.GetPerkLineLevel(InfBuddy._researchLine2[DynelManager.LocalPlayer.Profession])));

            //            _line1 = false;
            //            _line2 = true;
            //            return;
            //        }
            //        else
            //        {
            //            DynelManager.LocalPlayer.SetStat(Stat.PersonalResearchGoal,
            //                InfBuddy.GetHashLine2(Perk.GetPerkLineLevel(InfBuddy._researchLine2[DynelManager.LocalPlayer.Profession])));

            //            _line2 = true;
            //            return;
            //        }
            //    }
            //    if (InfBuddy._research["Line3"].AsBool() && _line3 == false)
            //    {
            //        if (_line2)
            //        {
            //            DynelManager.LocalPlayer.SetStat(Stat.PersonalResearchGoal,
            //                InfBuddy.GetHashLine3(Perk.GetPerkLineLevel(InfBuddy._researchLine3[DynelManager.LocalPlayer.Profession])));

            //            _line2 = false;
            //            _line3 = true;
            //            return;
            //        }
            //        else
            //        {
            //            DynelManager.LocalPlayer.SetStat(Stat.PersonalResearchGoal,
            //                InfBuddy.GetHashLine3(Perk.GetPerkLineLevel(InfBuddy._researchLine3[DynelManager.LocalPlayer.Profession])));

            //            _line3 = true;
            //            return;
            //        }
            //    }
            //    if (InfBuddy._research["Line4"].AsBool() && _line4 == false)
            //    {
            //        if (_line3)
            //        {
            //            DynelManager.LocalPlayer.SetStat(Stat.PersonalResearchGoal,
            //                InfBuddy.GetHashLine4(Perk.GetPerkLineLevel(InfBuddy._researchLine4[DynelManager.LocalPlayer.Profession])));

            //            _line3 = false;
            //            _line4 = true;
            //            return;
            //        }
            //        else
            //        {
            //            DynelManager.LocalPlayer.SetStat(Stat.PersonalResearchGoal,
            //                InfBuddy.GetHashLine4(Perk.GetPerkLineLevel(InfBuddy._researchLine4[DynelManager.LocalPlayer.Profession])));

            //            _line4 = true;
            //            return;
            //        }
            //    }
            //    if (InfBuddy._research["Line5"].AsBool() && _line5 == false)
            //    {
            //        if (_line4)
            //        {
            //            DynelManager.LocalPlayer.SetStat(Stat.PersonalResearchGoal,
            //                InfBuddy.GetHashLine5(Perk.GetPerkLineLevel(InfBuddy._researchLine5[DynelManager.LocalPlayer.Profession])));

            //            _line4 = false;
            //            _line5 = true;
            //            return;
            //        }
            //        else
            //        {
            //            DynelManager.LocalPlayer.SetStat(Stat.PersonalResearchGoal,
            //                InfBuddy.GetHashLine5(Perk.GetPerkLineLevel(InfBuddy._researchLine5[DynelManager.LocalPlayer.Profession])));

            //            _line5 = true;
            //            return;
            //        }
            //    }
            //    if (InfBuddy._research["Line6"].AsBool() && _line6 == false)
            //    {
            //        if (_line5)
            //        {
            //            DynelManager.LocalPlayer.SetStat(Stat.PersonalResearchGoal,
            //                InfBuddy.GetHashLine6(Perk.GetPerkLineLevel(InfBuddy._researchLine6[DynelManager.LocalPlayer.Profession])));

            //            _line5 = false;
            //            _line6 = true;
            //            return;
            //        }
            //        else
            //        {
            //            DynelManager.LocalPlayer.SetStat(Stat.PersonalResearchGoal,
            //                InfBuddy.GetHashLine6(Perk.GetPerkLineLevel(InfBuddy._researchLine6[DynelManager.LocalPlayer.Profession])));

            //            _line6 = true;
            //            return;
            //        }
            //    }
            //    if (InfBuddy._research["Line7"].AsBool() && _line7 == false)
            //    {
            //        if (_line6)
            //        {
            //            DynelManager.LocalPlayer.SetStat(Stat.PersonalResearchGoal,
            //                InfBuddy.GetHashLine7(Perk.GetPerkLineLevel(InfBuddy._researchLine7[DynelManager.LocalPlayer.Profession])));

            //            _line6 = false;
            //            _line7 = true;
            //            return;
            //        }
            //        else
            //        {
            //            DynelManager.LocalPlayer.SetStat(Stat.PersonalResearchGoal,
            //                InfBuddy.GetHashLine7(Perk.GetPerkLineLevel(InfBuddy._researchLine7[DynelManager.LocalPlayer.Profession])));

            //            _line7 = true;
            //            return;
            //        }
            //    }
            //    if (InfBuddy._research["Line8"].AsBool() && _line8 == false)
            //    {
            //        if (_line7)
            //        {
            //            DynelManager.LocalPlayer.SetStat(Stat.PersonalResearchGoal,
            //                InfBuddy.GetHashLine8(Perk.GetPerkLineLevel(InfBuddy._researchLine8[DynelManager.LocalPlayer.Profession])));

            //            _line7 = false;
            //            _line8 = true;
            //            return;
            //        }
            //    }
            //}
        }

        public void Tick()
        {
        }
    }
}
