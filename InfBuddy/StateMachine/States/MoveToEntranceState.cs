﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace InfBuddy
{
    public class MoveToEntranceState : IState
    {
        private const int MinWait = 5;
        private const int MaxWait = 15;

        private CancellationTokenSource _cancellationToken = new CancellationTokenSource();

        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == Constants.OmniPandeGId)
            {
                InfBuddy.NavMeshMovementController.Halt();
                return new DiedState();
            }

            if (Playfield.ModelIdentity.Instance == Constants.ClanPandeGId)
            {
                InfBuddy.NavMeshMovementController.Halt();
                return new DiedState();
            }

            if (Time.NormalTime - InfBuddy._stateTimeOut > 75f)
            {
                if (!InfBuddy._firstTimeout)
                {
                    InfBuddy._firstTimeout = true;
                    InfBuddy._secondTimeout = false;
                    InfBuddy.NavMeshMovementController.Halt();
                    InfBuddy.NavMeshMovementController.SetNavMeshDestination(new Vector3(2717.5f, 24.6f, 3327.4f));
                    return new MoveToEntranceState();
                }
                else if (!InfBuddy._secondTimeout)
                {
                    InfBuddy._firstTimeout = false;
                    InfBuddy._secondTimeout = true;
                    InfBuddy.NavMeshMovementController.Halt();
                    InfBuddy.NavMeshMovementController.SetNavMeshDestination(new Vector3(2765.7f, 24.6f, 3322.6f));
                    return new MoveToEntranceState();
                }
            }

            if (Playfield.ModelIdentity.Instance == Constants.NewInfMissionId)
            {
                if (Team.IsLeader || !InfBuddy.Config.IsLeech)
                    return new MoveToQuestStarterState();
                else
                    return new LeechState();
            }

            //if (Time.NormalTime > InfBuddy.CorrectionPathTimer + 80f)
            //{
            //    InfBuddy.NavMeshMovementController.Halt();
            //    return new MoveToQuestGiverState();
            //}

            return null;
        }

        public void OnStateEnter()
        {
            InfBuddy._stateTimeOut = Time.NormalTime;

            int randomWait = Utils.Next(MinWait, MaxWait);
            Chat.WriteLine($"Idling for {randomWait} seconds..");

            Task.Delay(randomWait * 1000).ContinueWith(x =>
            {
                InfBuddy.NavMeshMovementController.SetNavMeshDestination(Constants.EntrancePos);
                InfBuddy.NavMeshMovementController.AppendDestination(Constants.EntranceFinalPos);
            }, _cancellationToken.Token);
        }

        public void OnStateExit()
        {
            //InfBuddy.SetTimer = false;
            //Chat.WriteLine("MoveToEntranceState::OnStateExit");
            _cancellationToken.Cancel();
        }

        public void Tick()
        {
        }
    }
}
