﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using NavmeshMovementController;
using System;
using System.Threading;
using System.Threading.Tasks;


namespace InfBuddy
{
    public class MoveToQuestGiverState : IState
    {
        private const int Entropy = 2;
        private const int MinWait = 10;
        private const int MaxWait = 45;

        private bool MoveStarted = false;

        private CancellationTokenSource _cancellationToken = new CancellationTokenSource();
        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == Constants.OmniPandeGId)
            {
                InfBuddy.NavMeshMovementController.Halt();
                return new DiedState();
            }

            if (Playfield.ModelIdentity.Instance == Constants.ClanPandeGId)
            {
                InfBuddy.NavMeshMovementController.Halt();
                return new DiedState();
            }

            if (Time.NormalTime - InfBuddy._stateTimeOut > 75f)
            {
                if (!InfBuddy._firstTimeout)
                {
                    InfBuddy._firstTimeout = true;
                    InfBuddy._secondTimeout = false;
                    InfBuddy.NavMeshMovementController.Halt();
                    InfBuddy.NavMeshMovementController.SetNavMeshDestination(new Vector3(2717.5f, 24.6f, 3327.4f));
                    return new MoveToQuestGiverState();
                }
                else if (!InfBuddy._secondTimeout)
                {
                    InfBuddy._firstTimeout = false;
                    InfBuddy._secondTimeout = true;
                    InfBuddy.NavMeshMovementController.Halt();
                    InfBuddy.NavMeshMovementController.SetNavMeshDestination(new Vector3(2765.7f, 24.6f, 3322.6f));
                    return new MoveToQuestGiverState();
                }
            }

            if (!InfBuddy.NavMeshMovementController.IsNavigating && IsAtQuestGiver())
                return new GrabMissionState();

            return null;
        }

        public void OnStateEnter()
        {
            InfBuddy._stateTimeOut = Time.NormalTime;

            //Chat.WriteLine("MoveToQuestGiverState::OnStateEnter");

            if (!IsAtQuestGiver())
            {
                Vector3 randoPos = Constants.QuestGiverPos;
                randoPos.AddRandomness(Entropy);

                int randomWait = Utils.Next(MinWait, MaxWait);
                Chat.WriteLine($"Idling for {randomWait} seconds..");
                Task.Delay(randomWait * 1000).ContinueWith(x =>
                {
                    try
                    {
                        InfBuddy.NavMeshMovementController.SetNavMeshDestination(randoPos);
                    }
                    catch (Exception e)
                    {
                        InfBuddy.NavMeshMovementController.SetDestination(Constants.WallBugFixSpot);
                        InfBuddy.NavMeshMovementController.AppendNavMeshDestination(randoPos);
                    }
                }, _cancellationToken.Token);
            }
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("MoveToQuestGiverState::OnStateExit");
            _cancellationToken.Cancel();
        }

        public void Tick()
        {
        }

        private bool IsAtQuestGiver()
        {
            return DynelManager.LocalPlayer.Position.DistanceFrom(Constants.QuestGiverPos) < 6f;
        }
    }
}
