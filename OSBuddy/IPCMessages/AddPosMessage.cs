﻿using System;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace OSBuddy.IPCMessages
{
    [AoContract((int)IPCOpcode.AddPos)]
    public class AddPosMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.AddPos;
    }
}
