﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OSBuddy.IPCMessages
{
    public enum IPCOpcode
    {
        Start = 1201,
        Stop = 1202,
        AddPos = 1203,
        RespawnDelay = 1204
    }
}
