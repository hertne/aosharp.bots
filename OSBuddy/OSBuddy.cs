﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Core.Movement;
using AOSharp.Common.GameData;
using System.IO;
using AOSharp.Core.GameData;
using AOSharp.Core.UI.Options;
using AOSharp.Pathfinding;
using System.Data;
using AOSharp.Core.IPC;
using OSBuddy.IPCMessages;
using AOSharp.Core.Inventory;
using System.Collections.Concurrent;
using AOSharp.Common.GameData.UI;

namespace OSBuddy
{
    public class OSBuddy : AOPluginEntry
    {
        public static StateMachine _stateMachine;
        public static IPCChannel IPCChannel { get; private set; }
        public static Config Config { get; private set; }

        public static Identity Leader = Identity.None;
        public static bool IsLeader = false;

        public static string PluginDirectory;


        private static Settings OSBuddySettings = new Settings("OSBuddy");

        public static Window infoWindow;

        public static bool Running = false;

        public static bool SwitchForLastPos = false;
        public static bool MobsAllDead = false;
        public static bool Slam = false;
        public static bool Demo = true;

        public double _refreshAbsorbTimer;

        public static Vector3 currentPos;
        public static Vector3 lastPos;
        public static List<Vector3> RoamingVectors = new List<Vector3>();

        private static Settings settings = new Settings("OSBuddy");

        public override void Run(string pluginDir)
        {
            try
            {
                PluginDirectory = pluginDir;

                Chat.WriteLine("OSBuddy Loaded!");
                Chat.WriteLine("/osbuddy for settings.");

                Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\OSBuddy\\{Game.ClientInst}\\Config.json");

                IPCChannel = new IPCChannel(Convert.ToByte(Config.IPCChannel));

                _stateMachine = new StateMachine(new IdleState());

                IPCChannel = new IPCChannel(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));

                OSBuddySettings.AddVariable("Running", false);

                OSBuddySettings.AddVariable("Slam", false);
                OSBuddySettings.AddVariable("Demolish", false);

                OSBuddySettings["Running"] = false;

                SettingsController.RegisterSettingsWindow("OSBuddy", pluginDir + "\\UI\\OSBuddySettingWindow.xml", OSBuddySettings);

                IPCChannel.RegisterCallback((int)IPCOpcode.Start, OnStartPathMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.Stop, OnStopPathMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.AddPos, OnAddPosMessage);

                Chat.RegisterCommand("buddy", OSBuddyCommand);

                Game.OnUpdate += OnUpdate;
            }
            catch(Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        private void Start()
        {
            Running = true;

            OSBuddySettings["Running"] = true;

            if (!IsLeader && !(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());
        }

        private void Stop()
        {
            Running = false;

            OSBuddySettings["Running"] = false;

            _stateMachine.SetState(new IdleState());
        }


        private void OnStartPathMessage(int sender, IPCMessage msg)
        {
            StartMessage startMsg = (StartMessage)msg;

            Leader = new Identity(IdentityType.SimpleChar, sender);

            Start();
        }

        private void OnStopPathMessage(int sender, IPCMessage msg)
        {
            StopMessage stopMsg = (StopMessage)msg;

            Stop();
        }


        private void AddPos()
        {
            Chat.WriteLine("Waypoint Added.");

            RoamingVectors.Add(DynelManager.LocalPlayer.Position);
        }

        private void OnAddPosMessage(int sender, IPCMessage msg)
        {
            AddPosMessage addposMsg = (AddPosMessage)msg;

            AddPos();
        }

        private void InfoView(object s, ButtonBase button)
        {
            infoWindow = Window.CreateFromXml("Info", PluginDirectory + "\\UI\\OSBuddyInfoView.xml",
                windowSize: new Rect(0, 0, 455, 345),
                windowStyle: WindowStyle.Default,
                windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);
                infoWindow.Show(true);
        }

        private void AddWaypoint(object s, ButtonBase button)
        {
            RoamingVectors.Add(DynelManager.LocalPlayer.Position);
        }

        private void ClearWaypoints(object s, ButtonBase button)
        {
            RoamingVectors.Clear();
        }


        private void OnUpdate(object s, float deltaTime)
        {
            if (Game.IsZoning)
                return;

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                SettingsController.settingsWindow.FindView("ChannelBox", out TextInputView textinput1);
                SettingsController.settingsWindow.FindView("RespawnBox", out TextInputView textinput2);

                if (textinput1 != null && textinput1.Text != String.Empty)
                {
                    if (int.TryParse(textinput1.Text, out int channelValue))
                    {
                        if (Config.CharSettings[Game.ClientInst].IPCChannel != channelValue)
                        {
                            IPCChannel.SetChannelId(Convert.ToByte(channelValue));
                            Config.CharSettings[Game.ClientInst].IPCChannel = Convert.ToByte(channelValue);
                            SettingsController.OSBuddyChannel = channelValue.ToString();
                            Config.Save();
                        }
                    }
                }

                if (textinput2 != null && textinput2.Text != String.Empty)
                {
                    if (int.TryParse(textinput2.Text, out int rangeValue))
                    {
                        if (Config.CharSettings[Game.ClientInst].RespawnDelay != rangeValue)
                        {
                            Config.CharSettings[Game.ClientInst].RespawnDelay = rangeValue;
                            SettingsController.OSBuddyRespawnDelay = rangeValue.ToString();
                            Config.Save();
                        }
                    }
                }

                if (SettingsController.settingsView != null)
                {
                    if (SettingsController.settingsView.FindChild("OSBuddyInfoView", out Button infoView))
                    {
                        infoView.Tag = SettingsController.settingsView;
                        infoView.Clicked = InfoView;
                    }

                    if (SettingsController.settingsView.FindChild("AddWaypoint", out Button addBox))
                    {
                        addBox.Tag = SettingsController.settingsView;
                        addBox.Clicked = AddWaypoint;
                    }

                    if (SettingsController.settingsView.FindChild("ClearWaypoints", out Button clearBox))
                    {
                        clearBox.Tag = SettingsController.settingsView;
                        clearBox.Clicked = ClearWaypoints;
                    }
                }

                if (!OSBuddySettings["Demolish"].AsBool() && !OSBuddySettings["Slam"].AsBool()
                    && Running == true)
                {
                    Chat.WriteLine("No mode selected.");
                    OSBuddySettings["Running"] = false;
                    Running = false;
                    return;
                }

                if (OSBuddySettings["Demolish"].AsBool() && Config.CharSettings[Game.ClientInst].Demolish != true)
                {
                    Config.CharSettings[Game.ClientInst].Demolish = true;
                    Config.Save();
                    return;
                }
                if (!OSBuddySettings["Demolish"].AsBool() && Config.CharSettings[Game.ClientInst].Demolish == true)
                {
                    Config.CharSettings[Game.ClientInst].Demolish = false;
                    Config.Save();
                    return;
                }

                if (OSBuddySettings["Slam"].AsBool() && Config.CharSettings[Game.ClientInst].Slam != true)
                {
                    Config.CharSettings[Game.ClientInst].Slam = true;
                    Config.Save();
                    return;
                }
                if (!OSBuddySettings["Slam"].AsBool() && Config.CharSettings[Game.ClientInst].Slam == true)
                {
                    Config.CharSettings[Game.ClientInst].Slam = false;
                    Config.Save();
                    return;
                }

                if (!OSBuddySettings["Running"].AsBool() && Running == true)
                {
                    Stop();
                    IPCChannel.Broadcast(new StopMessage());
                    return;
                }
                if (OSBuddySettings["Running"].AsBool() && Running == false)
                {
                    if (OSBuddySettings["Demolish"].AsBool() && OSBuddySettings["Slam"].AsBool())
                    {
                        OSBuddySettings["Demolish"] = false;
                        OSBuddySettings["Slam"] = false;
                        OSBuddySettings["Running"] = false;

                        Chat.WriteLine($"Can only toggle one.");
                        return;
                    }

                    IsLeader = true;
                    Leader = DynelManager.LocalPlayer.Identity;

                    if (DynelManager.LocalPlayer.Identity == Leader)
                    {
                        IPCChannel.Broadcast(new StartMessage());
                    }
                    Start();
                    return;

                }
            }

            if (SettingsController.OSBuddyChannel == String.Empty)
            {
                SettingsController.OSBuddyChannel = Config.IPCChannel.ToString();
            }

            if (SettingsController.OSBuddyRespawnDelay == String.Empty)
            {
                SettingsController.OSBuddyRespawnDelay = Config.RespawnDelay.ToString();
            }

            _stateMachine.Tick();

        }

        private void OSBuddyCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                if (param.Length < 1)
                {
                    if (!OSBuddySettings["Demolish"].AsBool() && !OSBuddySettings["Slam"].AsBool())
                    {
                        Chat.WriteLine("No mode selected.");
                        OSBuddySettings["Running"] = false;
                        Running = false;
                        return;
                    }
                    if (OSBuddySettings["Demolish"].AsBool() && OSBuddySettings["Slam"].AsBool())
                    {
                        OSBuddySettings["Demolish"] = false;
                        OSBuddySettings["Slam"] = false;
                        OSBuddySettings["Running"] = false;

                        Chat.WriteLine($"Can only toggle one.");
                        return;
                    }
                    if (!OSBuddySettings["Running"].AsBool() && !Running)
                    {
                        IsLeader = true;
                        Leader = DynelManager.LocalPlayer.Identity;

                        if (DynelManager.LocalPlayer.Identity == Leader)
                        {
                            IPCChannel.Broadcast(new StartMessage());
                        }
                        Start();
                        Chat.WriteLine("Bot enabled.");
                        return;
                    }
                    else if (OSBuddySettings["Running"].AsBool() && Running)
                    {
                        Stop();
                        Chat.WriteLine("Bot disabled.");
                        IPCChannel.Broadcast(new StopMessage());
                        return;
                    }
                }

                switch (param[0].ToLower())
                {
                    case "addpos":
                        RoamingVectors.Add(DynelManager.LocalPlayer.Position);
                        break;

                    default:
                        return;
                }
                Config.Save();
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }
    }
}
