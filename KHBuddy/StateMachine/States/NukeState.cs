﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.IPC;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using KHBuddy.IPCMessages;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace KHBuddy
{
    public class NukeState : IState
    {
        public const double RefreshMongoTime = 8f;
        public const double RefreshAbsorbTime = 11f;

        public double _refreshMongoTimer;
        public double _refreshAbsorbTimer;

        public static List<Corpse> _hecksCorpsesAtPosEast;
        public static List<Corpse> _hecksCorpsesAtPosWest;
        public static List<Corpse> _hecksCorpsesAtPosBeach;
        public static List<SimpleChar> _hecks;

        Spell aoenukes = null;
        Spell absorb = null;

        public IState GetNextState()
        {
            _hecks = DynelManager.NPCs
                .Where(x => x.Name.Contains("Heckler") || x.Name.Contains("Voracious"))
                .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 39f
                    && x.IsAlive && x.IsInLineOfSight
                    && !x.IsMoving
                    && x.FightingTarget != null)
                .ToList();

            if (Time.NormalTime - KHBuddy._stateTimeOut > 1800f
                && DynelManager.LocalPlayer.Profession == Profession.NanoTechnician)
            {
                KHBuddy.KHBuddySettings["Toggle"] = false;
                Chat.WriteLine("Turning off bot, Idle for too long.");
                return new IdleState();
            }

            if (KHBuddy.SideSelection.Beach == (KHBuddy.SideSelection)KHBuddy.KHBuddySettings["SideSelection"].AsInt32()
                && DynelManager.LocalPlayer.Profession == Profession.Enforcer)
            {
                _hecksCorpsesAtPosBeach = DynelManager.Corpses
                    .Where(x => x.Name.Contains("Heckler") || x.Name.Contains("Voracious")
                        && x.DistanceFrom(DynelManager.LocalPlayer) <= 45f
                        && x.Position.DistanceFrom(new Vector3(901.9f, 4.4f, 299.6f)) < 8f)
                    .ToList();

                if (_hecks.Count == 0 && _hecksCorpsesAtPosBeach.Count >= 3)
                {
                    KHBuddy.RespawnTime = KHBuddy.GameTime.AddSeconds(720);
                    PullState._counterVec = 0;
                    return new PullState();
                }
            }

            if (KHBuddy.SideSelection.East == (KHBuddy.SideSelection)KHBuddy.KHBuddySettings["SideSelection"].AsInt32()
                && DynelManager.LocalPlayer.Profession == Profession.Enforcer)
            {
                _hecksCorpsesAtPosEast = DynelManager.Corpses
                    .Where(x => x.Name.Contains("Heckler") || x.Name.Contains("Voracious")
                        && x.DistanceFrom(DynelManager.LocalPlayer) <= 45f
                        && x.Position.DistanceFrom(new Vector3(1115.9f, 1.6f, 1064.3f)) < 8f)
                    .ToList();

                if (_hecks.Count == 0 && _hecksCorpsesAtPosEast.Count >= 3)
                {
                    KHBuddy.RespawnTime = KHBuddy.GameTime.AddSeconds(720);
                    PullState._counterVec = 0;
                    return new PullState();
                }
            }

            if (KHBuddy.SideSelection.West == (KHBuddy.SideSelection)KHBuddy.KHBuddySettings["SideSelection"].AsInt32()
                && DynelManager.LocalPlayer.Profession == Profession.Enforcer)
            {
                _hecksCorpsesAtPosWest = DynelManager.Corpses
                    .Where(x => x.Name.Contains("Heckler") || x.Name.Contains("Voracious")
                        && x.DistanceFrom(DynelManager.LocalPlayer) <= 45f
                        && x.Position.DistanceFrom(new Vector3(1043.2f, 1.6f, 1020.5f)) < 8f)
                    .ToList();

                if (_hecks.Count == 0 && _hecksCorpsesAtPosWest.Count >= 3)
                {
                    KHBuddy.RespawnTime = KHBuddy.GameTime.AddSeconds(720);
                    PullState._counterVec = 0;
                    return new PullState();
                }
            }

            if (KHBuddy.SideSelection.EastAndWest == (KHBuddy.SideSelection)KHBuddy.KHBuddySettings["SideSelection"].AsInt32()
                && DynelManager.LocalPlayer.Profession == Profession.Enforcer)
            {
                if (KHBuddy._doingEast)
                {
                    _hecksCorpsesAtPosEast = DynelManager.Corpses
                        .Where(x => x.Name.Contains("Heckler") || x.Name.Contains("Voracious")
                            && x.DistanceFrom(DynelManager.LocalPlayer) <= 45f
                            && x.Position.DistanceFrom(new Vector3(1115.9f, 1.6f, 1064.3f)) < 8f)
                        .ToList();

                    if (_hecks.Count == 0 && _hecksCorpsesAtPosEast.Count >= 3)
                    {
                        KHBuddy.RespawnTimeEast = KHBuddy.GameTime.AddSeconds(720);
                        KHBuddy._doingEast = false;
                        KHBuddy._doingWest = true;
                        KHBuddy.IPCChannel.Broadcast(new MoveWestMessage());
                        MovementController.Instance.SetPath(PullState.PathToWest);
                        return new PullState();
                    }
                }

                if (KHBuddy._doingWest)
                {
                    _hecksCorpsesAtPosWest = DynelManager.Corpses
                        .Where(x => x.Name.Contains("Heckler") || x.Name.Contains("Voracious")
                            && x.DistanceFrom(DynelManager.LocalPlayer) <= 45f
                            && x.Position.DistanceFrom(new Vector3(1043.2f, 1.6f, 1020.5f)) < 8f)
                        .ToList();

                    if (_hecks.Count == 0 && _hecksCorpsesAtPosWest.Count >= 3)
                    {
                        KHBuddy.RespawnTimeWest = KHBuddy.GameTime.AddSeconds(720);
                        KHBuddy._init = true;
                        KHBuddy._doingWest = false;
                        KHBuddy._doingEast = true;
                        KHBuddy.IPCChannel.Broadcast(new MoveEastMessage());
                        MovementController.Instance.SetPath(PullState.PathToEast);
                        return new PullState();
                    }
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            KHBuddy._stateTimeOut = Time.NormalTime;
            //Chat.WriteLine("NukeState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("NukeState::OnStateExit");
        }

        private static class RelevantNanos
        {
            public static readonly int[] AOENukes = {28638,
                266297, 28637, 28594, 45922, 45906, 45884, 28635, 28593, 45925, 45940, 45900, 28629,
                45917, 45937, 28599, 45894, 45943, 28633, 28631 };
        }

        public void Tick()
        {
            _hecks = DynelManager.NPCs
                .Where(x => x.Name.Contains("Heckler") || x.Name.Contains("Voracious"))
                .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 39f
                    && x.IsAlive && x.IsInLineOfSight
                    && !x.IsMoving
                    && x.FightingTarget != null)
                .ToList();

            if (DynelManager.LocalPlayer.Profession == Profession.NanoTechnician)
            {
                if (aoenukes == null)
                    aoenukes = Spell.List
                        .Where(x => RelevantNanos.AOENukes.Contains(x.Id))
                        .Where(x => x.MeetsSelfUseReqs())
                        .FirstOrDefault();

                if (KHBuddy.SideSelection.Beach == (KHBuddy.SideSelection)KHBuddy.KHBuddySettings["SideSelection"].AsInt32())
                {
                    List<SimpleChar> _hecksAtPosBeach = DynelManager.NPCs
                            .Where(x => x.Name.Contains("Heckler") || x.Name.Contains("Voracious"))
                            .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 43f)
                            .Where(x => x.IsAlive && x.IsInLineOfSight && x.IsAttacking
                                && !x.IsMoving
                                && x.FightingTarget.Identity != DynelManager.LocalPlayer.Identity
                                && x.Position.DistanceFrom(new Vector3(901.9f, 4.4f, 299.6f)) < 10f)
                            .ToList();

                    if (_hecksAtPosBeach.Count >= 1)
                    {
                        if (!Spell.HasPendingCast && aoenukes.IsReady 
                            && (DynelManager.LocalPlayer.NanoPercent >= 66 || DynelManager.LocalPlayer.HealthPercent >= 66))
                        {
                            aoenukes.Cast(_hecksAtPosBeach.FirstOrDefault(), true);
                            KHBuddy._stateTimeOut = Time.NormalTime;
                        }
                    }
                }

                if (KHBuddy.SideSelection.West == (KHBuddy.SideSelection)KHBuddy.KHBuddySettings["SideSelection"].AsInt32()
                    || KHBuddy.SideSelection.EastAndWest == (KHBuddy.SideSelection)KHBuddy.KHBuddySettings["SideSelection"].AsInt32())
                {
                    List<SimpleChar> _hecksAtPosWest = DynelManager.NPCs
                            .Where(x => x.Name.Contains("Heckler") || x.Name.Contains("Voracious"))
                            .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 60f)
                            .Where(x => x.IsAlive && x.IsInLineOfSight && x.IsAttacking
                                && !x.IsMoving
                                && x.FightingTarget.Identity != DynelManager.LocalPlayer.Identity
                                && x.Position.DistanceFrom(new Vector3(1043.2f, 1.6f, 1020.5f)) < 10f)
                            .ToList();


                    if (_hecksAtPosWest.Count >= 1)
                    {
                        if (!Spell.HasPendingCast && aoenukes.IsReady 
                            && (DynelManager.LocalPlayer.NanoPercent >= 66 || DynelManager.LocalPlayer.HealthPercent >= 66))
                        {
                            aoenukes.Cast(_hecksAtPosWest.FirstOrDefault(), true);
                            KHBuddy._stateTimeOut = Time.NormalTime;
                        }
                    }
                }

                if (KHBuddy.SideSelection.East == (KHBuddy.SideSelection)KHBuddy.KHBuddySettings["SideSelection"].AsInt32()
                    || KHBuddy.SideSelection.EastAndWest == (KHBuddy.SideSelection)KHBuddy.KHBuddySettings["SideSelection"].AsInt32())
                {
                    List<SimpleChar> _hecksAtPosEast = DynelManager.NPCs
                            .Where(x => x.Name.Contains("Heckler") || x.Name.Contains("Voracious"))
                            .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 60f)
                            .Where(x => x.IsAlive && x.IsInLineOfSight && x.IsAttacking
                                && !x.IsMoving
                                && x.FightingTarget.Identity != DynelManager.LocalPlayer.Identity
                                && x.Position.DistanceFrom(new Vector3(1115.9f, 1.6f, 1064.3f)) < 10f)
                            .ToList();

                    if (_hecksAtPosEast.Count >= 1)
                    {
                        if (!Spell.HasPendingCast && aoenukes.IsReady 
                            && (DynelManager.LocalPlayer.NanoPercent >= 66 || DynelManager.LocalPlayer.HealthPercent >= 66))
                        {
                            aoenukes.Cast(_hecksAtPosEast.FirstOrDefault(), true);
                            KHBuddy._stateTimeOut = Time.NormalTime;
                        }
                    }
                }
            }

            if (DynelManager.LocalPlayer.Profession == Profession.Enforcer)
            {
                Spell.Find(270786, out Spell mongobuff);

                if (absorb == null)
                    absorb = Spell.List.Where(x => x.Nanoline == NanoLine.AbsorbACBuff).OrderBy(x => x.StackingOrder).FirstOrDefault();

                if (_hecks.Count >= 1)
                {
                    if (!Spell.HasPendingCast && mongobuff.IsReady && Time.NormalTime > _refreshMongoTimer + RefreshMongoTime)
                    {
                        mongobuff.Cast();
                        _refreshMongoTimer = Time.NormalTime;
                    }
                    if (!Spell.HasPendingCast && absorb.IsReady && Time.NormalTime > _refreshAbsorbTimer + RefreshAbsorbTime)
                    {
                        absorb.Cast();
                        _refreshAbsorbTimer = Time.NormalTime;
                    }
                }
            }
        }
    }
}
