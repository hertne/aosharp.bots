﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttackBuddy
{
    public class IdleState : IState
    {
        public IState GetNextState()
        {
            if (AttackBuddy.Toggle == true)
            {
                if (AttackBuddy.AttackBuddySettings["Attack"].AsBool())
                {
                    return new ScanState();
                }
                if (AttackBuddy.AttackBuddySettings["Defend"].AsBool())
                {
                    return new DefendState();
                }
                if (AttackBuddy.AttackBuddySettings["Roam"].AsBool())
                {
                    return new RoamState();
                }
                if (AttackBuddy.AttackBuddySettings["Nuke"].AsBool())
                {
                    return new NukeState();
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("IdleState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("IdleState::OnStateExit");
        }

        public void Tick()
        {
        }
    }
}
