﻿using System;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace AttackBuddy.IPCMessages
{
    [AoContract((int)IPCOpcode.ThreatRange)]
    public class ThreatRangeMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.ThreatRange;

        [AoMember(0)]
        public int Range { get; set; }
    }
}
