﻿using System;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace AttackBuddy.IPCMessages
{
    [AoContract((int)IPCOpcode.StartMode)]
    public class StartModeMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.StartMode;

        [AoMember(0)]
        public bool Attack { get; set; }
        [AoMember(1)]
        public bool Defend { get; set; }
        [AoMember(2)]
        public bool Roam { get; set; }

        [AoMember(3)]
        public bool Nuke { get; set; }
    }
}
