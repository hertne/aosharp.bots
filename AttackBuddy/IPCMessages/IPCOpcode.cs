﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttackBuddy.IPCMessages
{
    public enum IPCOpcode
    {
        SetPos = 1001,
        SetResetPos = 1002,
        AddPos = 1003,
        SetAggroTool = 1004,
        SetAutoLoot = 1006,
        StartMode = 1007,
        StopMode = 1008,
        NoneSelected = 1009,
        ThreatRange = 1010
    }
}
