﻿using System;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace AttackBuddy.IPCMessages
{
    [AoContract((int)IPCOpcode.StopMode)]
    public class StopModeMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.StopMode;

        [AoMember(0)]
        public bool Attack { get; set; }
        [AoMember(1)]
        public bool Defend { get; set; }
        [AoMember(2)]
        public bool Roam { get; set; }

        [AoMember(3)]
        public bool Nuke { get; set; }
    }
}
