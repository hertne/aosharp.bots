﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Core.Movement;
using AOSharp.Core.IPC;
using AOSharp.Pathfinding;
using AXPBuddy.IPCMessages;
using AOSharp.Common.GameData;
using AOSharp.Core.Inventory;
using AOSharp.Common.GameData.UI;

namespace AXPBuddy
{
    public class AXPBuddy : AOPluginEntry
    {
        public static StateMachine _stateMachine;
        public static NavMeshMovementController NavMeshMovementController { get; private set; }
        public static IPCChannel IPCChannel { get; private set; }
        public static Config Config { get; private set; }

        public static Identity Leader = Identity.None;
        public static bool IsLeader = false;

        public static SimpleChar _attackTarget = null;

        public static List<SimpleChar> _mob = new List<SimpleChar>();

        public static bool Toggle = false;

        private static double _refreshList;
        public static double _stateTimeOut;

        public static string PluginDirectory;

        public static Settings _settings = new Settings("AXPBuddy");

        public static Window infoWindow;

        public static List<string> NamesToIgnore = new List<string>
        {
                    "Alien Fuel Tower",
                    "Kyr'Ozch Guardian",
                    "Engorged Sacrificial Husks",
                    "Honor Guard - Ilari'Uri",
                    "Representative of IPS",
                    "Transportation Officer Darren Plush",
                    "Unicorn Field Engineer",
                    "Buckethead Technodealer",
                    "Unicorn Commander Labbe",
                    "Professor Raji Masoud",
                    "Slimy Alien Plant",
                    "Awakened Xan",
                    "Altar of Torture",
                    "Altar of Purification",
                    "Calan-Cur",
                    "Spirit of Judgement",
                    "Wandering Spirit",
                    "Altar of Torture",
                    "Altar of Purification",
                    "Unicorn Coordinator Magnum Blaine",
                    "Xan Spirit",
                    "Watchful Spirit",
                    "Amesha Vizaresh",
                    "Guardian Spirit of Purification",
                    "Tibor 'Rocketman' Nagy",
                    "One Who Obeys Precepts",
                    "The Retainer Of Ergo",
                    "Outzone Supplier",
                    "Hollow Island Weed",
                    "Sheila Marlene",
                    "Unicorn Advance Sentry",
                    "Unicorn Technician",
                    "Basic Tools Merchant",
                    "Container Supplier",
                    "Basic Quality Pharmacist",
                    "Basic Quality Armorer",
                    "Basic Quality Weaponsdealer",
                    "Tailor",
                    "Unicorn Commander Rufus",
                    "Ergo, Inferno Guardian of Shadows",
                    "Unicorn Trooper",
                    "Unicorn Squadleader",
                    "Rookie Alien Hunter",
                    "Unicorn Service Tower Alpha",
                    "Unicorn Service Tower Delta",
                    "Unicorn Service Tower Gamma",
                    "Sean Powell",
                    "Xan Spirit",
                    "Unicorn Guard",
                    "Essence Fragment",
                    "Scalding Flames",
                    "Guide",
                    "Searing Flame",
                    "Searing Flames",
                    "Guard",
                    "Awakened Xan",
                    "Fourth Watch Down",
                    "Mortar Bombardment",
                    "Dogmatic Pestilence",
                    "Flame of the Immortal One",
                    "Calan-Cur",
                    "Pulsating Ice Obelisk",
                    "Unnatural Ice"
        };

        public static List<Vector3> UnicornHubPath = new List<Vector3>
        {
            new Vector3(140.3f, 100.9f, 178.0f),
            new Vector3(138.9f, 100.9f, 285.4f),
            new Vector3(108.0f, 100.9f, 274.7f),
            new Vector3(109.3f, 100.8f, 232.7f),
            new Vector3(70.5f, 101.0f, 232.0f)
        };

        public override void Run(string pluginDir)
        {
            try
            {
                PluginDirectory = pluginDir;

                Chat.WriteLine("AXPBuddy Loaded!");
                Chat.WriteLine("/axpbuddy for settings.");

                Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\AXPBuddy\\{Game.ClientInst}\\Config.json");

                IPCChannel = new IPCChannel(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));

                _settings.AddVariable("Toggle", false);
                _settings.AddVariable("Merge", false);

                _settings.AddVariable("Leecher", false);
                _settings.AddVariable("AggroTool", false);

                _settings["Toggle"] = false;

                SettingsController.RegisterSettingsWindow("AXPBuddy", pluginDir + "\\UI\\AXPBuddySettingWindow.xml", _settings);

                _stateMachine = new StateMachine(new IdleState());

                NavMeshMovementController = new NavMeshMovementController($"{pluginDir}\\NavMeshes", true);
                MovementController.Set(NavMeshMovementController);

                Chat.RegisterCommand("buddy", APFBuddyCommand);

                IPCChannel.RegisterCallback((int)IPCOpcode.Start, OnStartMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.Stop, OnStopMessage);

                Team.TeamRequest += OnTeamRequest;
                Game.OnUpdate += OnUpdate;
                //Game.TeleportEnded += OnZoned;
                DynelManager.DynelSpawned += OnDynelSpawned;
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }

        private void OnDynelSpawned(object _, Dynel dynel)
        {
            _mob = DynelManager.NPCs
                .Where(c => c.Position.DistanceFrom(AXPBuddy.GetLeader().Position) < 30f
                        && !c.IsPet
                        && !c.IsPlayer
                        && c.Health > 0
                        && c.IsAlive && c.IsInLineOfSight
                        && !NamesToIgnore.Contains(c.Name)
                        && c.Name != "Zix" && c.Name != "Buckethead Technodealer")
                .OrderBy(c => c.Position.DistanceFrom(AXPBuddy.GetLeader().Position))
                .OrderBy(c => c.HealthPercent)
                .ToList();
        }

        public static SimpleChar GetLeader()
        {
            if (Leader != Identity.None)
            {
                SimpleChar LeaderScan = DynelManager.Players
                    .Where(c => c.Identity == Leader && c.DistanceFrom(DynelManager.LocalPlayer) < 45f
                        && c.IsValid)
                    .FirstOrDefault();

                if (LeaderScan != null) { return LeaderScan; }
                else
                    return DynelManager.LocalPlayer;
            }

            return DynelManager.LocalPlayer;
        }

        public static void Start()
        {
            if (!Toggle)
                Toggle = true;

            if (!(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());

            if (IsLeader)
            {
                if (!_settings["Toggle"].AsBool())
                    _settings["Toggle"] = true;
            }
        }

        private void Stop()
        {
            Toggle = false;

            _settings["Toggle"] = false;

            NavMeshMovementController.Halt();
        }

        private void OnStartMessage(int sender, IPCMessage msg)
        {
            StartMessage startMsg = (StartMessage)msg;

            //Chat.WriteLine("OnStartMessage");

            if (!_settings["Toggle"].AsBool())
                _settings["Toggle"] = true;

            Leader = new Identity(IdentityType.SimpleChar, sender);
            Start();
        }

        private void OnStopMessage(int sender, IPCMessage msg)
        {
            //Chat.WriteLine("OnStopMessage");
            Stop();
        }

        private void InfoView(object s, ButtonBase button)
        {
            infoWindow = Window.CreateFromXml("Info", PluginDirectory + "\\UI\\AXPBuddyInfoView.xml",
                windowSize: new Rect(0, 0, 220, 310),
                windowStyle: WindowStyle.Default,
                windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);

            infoWindow.Show(true);
        }


        private void OnUpdate(object s, float deltaTime)
        {
            if (Game.IsZoning)
                return;

            if (Time.NormalTime - _refreshList >= 1)
            {
                _mob = DynelManager.NPCs
                .Where(c => c.Position.DistanceFrom(AXPBuddy.GetLeader().Position) < 30f
                        && !c.IsPet
                        && !c.IsPlayer
                        && c.Health > 0
                        && c.IsAlive && c.IsInLineOfSight
                        && !NamesToIgnore.Contains(c.Name)
                        && c.Name != "Zix" && c.Name != "Buckethead Technodealer")
                .OrderBy(c => c.Position.DistanceFrom(AXPBuddy.GetLeader().Position))
                .OrderBy(c => c.HealthPercent)
                .ToList();

                _refreshList = Time.NormalTime;
            }

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                SettingsController.settingsWindow.FindView("ChannelBox", out TextInputView textinput1);

                if (textinput1 != null && textinput1.Text != String.Empty)
                {
                    if (int.TryParse(textinput1.Text, out int channelValue))
                    {
                        if (Config.CharSettings[Game.ClientInst].IPCChannel != channelValue)
                        {
                            IPCChannel.SetChannelId(Convert.ToByte(channelValue));
                            Config.CharSettings[Game.ClientInst].IPCChannel = Convert.ToByte(channelValue);
                            SettingsController.AXPBuddyChannel = channelValue.ToString();
                            Config.Save();
                        }
                    }
                }

                if (SettingsController.settingsWindow.FindView("AXPBuddyInfoView", out Button infoView))
                {
                    infoView.Tag = SettingsController.settingsWindow;
                    infoView.Clicked = InfoView;
                }

                if (!_settings["Toggle"].AsBool() && Toggle == true)
                {
                    Stop();
                    IPCChannel.Broadcast(new StopMessage());
                    return;
                }
                if (_settings["Toggle"].AsBool() && Toggle == false)
                {
                    IsLeader = true;
                    Leader = DynelManager.LocalPlayer.Identity;

                    _settings["Toggle"] = true;

                    Start();
                    IPCChannel.Broadcast(new StartMessage());
                    return;
                }

                if (_settings["Leecher"].AsBool() && Config.CharSettings[Game.ClientInst].IsLeech != true)
                {
                    Config.CharSettings[Game.ClientInst].IsLeech = true;
                    Config.Save();
                    return;
                }
                if (!_settings["Leecher"].AsBool() && Config.CharSettings[Game.ClientInst].IsLeech == true)
                {
                    Config.CharSettings[Game.ClientInst].IsLeech = false;
                    Config.Save();
                    return;
                }

                if (_settings["AggroTool"].AsBool() && !Config.CharSettings[Game.ClientInst].AggroTool)
                {
                    Config.CharSettings[Game.ClientInst].AggroTool = true;
                    Config.Save();
                    return;
                }
                if (!_settings["AggroTool"].AsBool() && Config.CharSettings[Game.ClientInst].AggroTool)
                {
                    Config.CharSettings[Game.ClientInst].AggroTool = false;
                    Config.Save();
                    return;
                }
            }

            if (SettingsController.AXPBuddyChannel == String.Empty)
            {
                SettingsController.AXPBuddyChannel = Config.IPCChannel.ToString();
            }

            _stateMachine.Tick();
        }

        private void OnTeamRequest(object s, TeamRequestEventArgs e)
        {
            if (e.Requester != Leader)
            {
                if (Toggle)
                    e.Ignore();

                return;
            }

            e.Accept();
        }

        private void APFBuddyCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                if (param.Length < 1)
                {
                    if (!_settings["Toggle"].AsBool() && !Toggle)
                    {
                        IsLeader = true;
                        Leader = DynelManager.LocalPlayer.Identity;

                        if (DynelManager.LocalPlayer.Identity == Leader)
                        {
                            IPCChannel.Broadcast(new StartMessage());
                        }
                        _settings["Toggle"] = true;

                        Start();
                        Chat.WriteLine("Starting");

                    }
                    else
                    {
                        Stop();
                        Chat.WriteLine("Stopping");
                        IPCChannel.Broadcast(new StopMessage());
                    }
                    return;
                }
                Config.Save();
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }
    }
}
