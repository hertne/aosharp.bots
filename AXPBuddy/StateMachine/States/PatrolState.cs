﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AXPBuddy
{
    public class PatrolState : IState
    {
        //private static double _pathingTimeout = Time.NormalTime;

        private static bool _missingPerson = false;
        private static bool _missingLeader = false;

        public IState GetNextState()
        {
            if (Team.IsLeader
                && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.S13GoalPos) <= 8f
                && AXPBuddy._mob.Count == 0)
            {
                if (AXPBuddy._settings["Toggle"].AsBool())
                {
                    AXPBuddy.NavMeshMovementController.Halt();
                    return new ReformState();
                }
            }

            if ((!Team.IsLeader || AXPBuddy._settings["Merge"].AsBool())
                && Playfield.ModelIdentity.Instance == Constants.APFHubId
                && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.S13ZoneOutPos) <= 10f)
            {
                if (AXPBuddy._settings["Toggle"].AsBool())
                {
                    return new ReformState();
                }
            }

            if (Playfield.ModelIdentity.Instance == Constants.APFHubId
                && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.S13ZoneOutPos) > 10f)
            {
                if (AXPBuddy._settings["Toggle"].AsBool())
                {
                    return new DiedState();
                }
            }

            if (Playfield.ModelIdentity.Instance == Constants.XanHubId)
            {
                if (AXPBuddy._settings["Toggle"].AsBool())
                {
                    return new DiedState();
                }
            }

            if (AXPBuddy._attackTarget != null)
                return new FightState();

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("PatrolState::OnStateEnter");
        }

        public void OnStateExit()
        {
           //Chat.WriteLine("PatrolState::OnStateExit");
        }

        public void Tick()
        {
            if (AXPBuddy._mob.Count >= 1 && AXPBuddy.IsLeader)
            {
                if (AXPBuddy._mob.FirstOrDefault().Health > 0)
                {
                    AXPBuddy._attackTarget = AXPBuddy._mob.FirstOrDefault();
                    return;
                }
            }

            if (AXPBuddy._mob.Count >= 1 && !AXPBuddy.IsLeader)
            {
                if (AXPBuddy.GetLeader().IsAttacking 
                    && AXPBuddy.GetLeader().FightingTarget != null)
                {
                    if (AXPBuddy.GetLeader().FightingTarget.Health > 0)
                    {
                        AXPBuddy._attackTarget = AXPBuddy.GetLeader().FightingTarget;
                        return;
                    }
                }
            }

            if (AXPBuddy._mob.Count >= 1) { return; }

            if (!_missingPerson
                && Team.Members.Any(c => c.Character == null)
                        || Team.Members
                            .Where(c => c.Character != null
                                && (c.Character.HealthPercent <= 65 || c.Character.NanoPercent <= 65))
                            .Count() >= 1
                        || Spell.HasPendingCast)
            {
                _missingPerson = true;
                return;
            }

            if (_missingPerson
                && !Team.Members.Any(c => c.Character == null)
                        && Team.Members
                            .Where(c => c.Character != null
                                && (c.Character.HealthPercent <= 65 || c.Character.NanoPercent <= 65))
                            .Count() == 0
                        && !Spell.HasPendingCast)
            {
                _missingPerson = false;
                return;
            }

            if (!AXPBuddy.NavMeshMovementController.IsNavigating
                && AXPBuddy.IsLeader && !_missingPerson)
            {
                AXPBuddy.NavMeshMovementController.SetNavMeshDestination(Constants.S13GoalPos);
            }

            if (!AXPBuddy.IsLeader
                && !AXPBuddy._settings["Merge"].AsBool())
            {
                SimpleChar LeaderScan = DynelManager.Players
                    .Where(c => c.Identity == AXPBuddy.Leader && c.DistanceFrom(DynelManager.LocalPlayer) < 70f)
                    .FirstOrDefault();

                if (LeaderScan == null)
                {
                    if (!AXPBuddy.NavMeshMovementController.IsNavigating)
                    {
                        _missingLeader = true;
                        AXPBuddy.NavMeshMovementController.SetNavMeshDestination(Constants.S13GoalPos);
                    }
                }
                else
                {
                    if (AXPBuddy.NavMeshMovementController.IsNavigating
                        && _missingLeader)
                    {
                        _missingLeader = false;
                        AXPBuddy.NavMeshMovementController.Halt();
                    }

                    if (DynelManager.LocalPlayer.Position.DistanceFrom(LeaderScan.Position) > 4f
                        && !AXPBuddy.NavMeshMovementController.IsNavigating
                        && !_missingLeader)
                    {
                        AXPBuddy.NavMeshMovementController.SetNavMeshDestination(LeaderScan.Position);
                    }
                }
            }
        }
    }
}
