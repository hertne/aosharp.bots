﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System.Collections.Generic;
using System.Linq;

namespace AXPBuddy
{
    public class LeechState : IState
    {
        private static int _current = 0;

        private static List<Vector3> _destinations = new List<Vector3>()
        {
            new Vector3(85.4f, 67.7f, 83.7f),
            new Vector3(111.3f, 67.6f, 153.6f),
            new Vector3(144.3f, 67.6f, 156.5f),
            new Vector3(211.5f, 67.6f, 168.3f),
            new Vector3(226.0f, 67.6f, 246.5f),
            new Vector3(118.4f, 67.6f, 254.8f),
            new Vector3(62.2f, 67.6f, 313.4f),
            new Vector3(83.1f, 67.6f, 467.1f),
            new Vector3(122.0f, 67.6f, 492.1f),
            new Vector3(155.9f, 67.6f, 491.4)
        };

        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == Constants.APFHubId
                && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.S13ZoneOutPos) <= 10f)
            {
                if (AXPBuddy._settings["Toggle"].AsBool())
                {
                    return new ReformState();
                }
            }

            if (Playfield.ModelIdentity.Instance == Constants.APFHubId
                && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.S13ZoneOutPos) > 10f)
            {
                if (AXPBuddy._settings["Toggle"].AsBool())
                {
                    return new DiedState();
                }
            }

            if (Playfield.ModelIdentity.Instance == Constants.XanHubId)
            {
                if (AXPBuddy._settings["Toggle"].AsBool())
                {
                    return new DiedState();
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("LeechState::OnStateEnter");

            DynelManager.LocalPlayer.Position = new Vector3(150.8, 67.7f, 42.0f);
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("LeechState::OnStateExit");
            _current = 0;
        }

        public void Tick()
        {
            SimpleChar LeaderScan = DynelManager.Players
                .Where(c => c.Identity == AXPBuddy.Leader && c.DistanceFrom(DynelManager.LocalPlayer) < 70f)
                .FirstOrDefault();

            if (_current == _destinations.Count - 1) { return; }

            if (DynelManager.LocalPlayer.Position.DistanceFrom(_destinations[_current]) <= 3f)
            {
                if (_current < _destinations.Count)
                    _current++;
            }

            if (LeaderScan == null)
            {
                if (DynelManager.LocalPlayer.Position.DistanceFrom(_destinations.Last()) > 1f)
                {
                    if (!AXPBuddy.NavMeshMovementController.IsNavigating)
                        AXPBuddy.NavMeshMovementController.SetNavMeshDestination(_destinations[_current]);
                }
            }
            else
            {
                if (AXPBuddy.NavMeshMovementController.IsNavigating)
                    AXPBuddy.NavMeshMovementController.Halt();
            }
        }
    }
}
