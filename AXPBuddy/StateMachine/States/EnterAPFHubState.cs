﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using AXPBuddy.IPCMessages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages.OrgServerMessages;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AXPBuddy
{
    public class EnterAPFHubState : IState
    {
        public IState GetNextState()
        {
            if (AXPBuddy._settings["Toggle"].AsBool())
            {
                if (Playfield.ModelIdentity.Instance == Constants.APFHubId)
                    return new EnterSectorState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("Died running back.");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("State exitted.");
        }

        public void Tick()
        {
            if (DynelManager.LocalPlayer.HealthPercent > 65 && DynelManager.LocalPlayer.NanoPercent > 65
                && Playfield.ModelIdentity.Instance == Constants.UnicornHubId && !AXPBuddy.NavMeshMovementController.IsNavigating)
            {
                Dynel Lever = DynelManager.AllDynels
                    .Where(c => c.Name == "A Lever")
                    .Where(c => c.DistanceFrom(DynelManager.LocalPlayer) < 6f)
                    .FirstOrDefault();

                if (Lever == null)
                    AXPBuddy.NavMeshMovementController.SetPath(AXPBuddy.UnicornHubPath);
                else
                    Lever.Use();
            }
        }
    }
}
