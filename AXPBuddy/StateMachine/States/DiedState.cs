﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using AXPBuddy.IPCMessages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages.OrgServerMessages;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AXPBuddy
{
    public class DiedState : IState
    {
        public IState GetNextState()
        {
            if (AXPBuddy._settings["Toggle"].AsBool())
            {
                if (Playfield.ModelIdentity.Instance == Constants.UnicornHubId)
                    return new EnterAPFHubState();

                if (Playfield.ModelIdentity.Instance == Constants.APFHubId)
                    return new EnterSectorState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine($"DiedState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("DiedState::OnStateExit");
        }

        public void Tick()
        {
            if (DynelManager.LocalPlayer.HealthPercent > 65 && DynelManager.LocalPlayer.NanoPercent > 65
                && Playfield.ModelIdentity.Instance == Constants.XanHubId && !AXPBuddy.NavMeshMovementController.IsNavigating)
            {
                AXPBuddy.NavMeshMovementController.SetDestination(Constants.XanHubPos);
            }
        }
    }
}
