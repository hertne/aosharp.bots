﻿using AOSharp.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using AOSharp.Common.GameData;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI;
using AOSharp.Core.IPC;
using AOSharp.Common.GameData.UI;
using System.Threading.Tasks;

namespace LootBuddy
{
    public class Main : AOPluginEntry
    {
        private double _lastCheckTime = Time.NormalTime;
        private AOSharp.Core.Settings LootBuddySettings = new AOSharp.Core.Settings("LootBuddy");

        public static Container extrabag1 = Inventory.Backpacks.FirstOrDefault(x => x.Name == "extra1");
        public static Container extrabag2 = Inventory.Backpacks.FirstOrDefault(x => x.Name == "extra2");
        public static Container extrabag3 = Inventory.Backpacks.FirstOrDefault(x => x.Name == "extra3");
        public static Container extrabag4 = Inventory.Backpacks.FirstOrDefault(x => x.Name == "extra4");
        public static Container extrabag5 = Inventory.Backpacks.FirstOrDefault(x => x.Name == "extra5");
        public static Container extrabag6 = Inventory.Backpacks.FirstOrDefault(x => x.Name == "extra6");
        public static Container extrabag7 = Inventory.Backpacks.FirstOrDefault(x => x.Name == "extra7");
        public static Container extrabag8 = Inventory.Backpacks.FirstOrDefault(x => x.Name == "extra8");
        public static Container extrabag9 = Inventory.Backpacks.FirstOrDefault(x => x.Name == "extra9");
        public static Container extrabag10 = Inventory.Backpacks.FirstOrDefault(x => x.Name == "extra10");
        public static Container extrabag11 = Inventory.Backpacks.FirstOrDefault(x => x.Name == "extra11");
        public static Container extrabag12 = Inventory.Backpacks.FirstOrDefault(x => x.Name == "extra12");
        public static Container extrabag13 = Inventory.Backpacks.FirstOrDefault(x => x.Name == "extra13");
        public static Container extrabag14 = Inventory.Backpacks.FirstOrDefault(x => x.Name == "extra14");
        public static Container extrabag15 = Inventory.Backpacks.FirstOrDefault(x => x.Name == "extra15");
        public static Container extrabag16 = Inventory.Backpacks.FirstOrDefault(x => x.Name == "extra16");
        public static Container extrabag17 = Inventory.Backpacks.FirstOrDefault(x => x.Name == "extra17");
        public static Container extrabag18 = Inventory.Backpacks.FirstOrDefault(x => x.Name == "extra18");

        public static Corpse corpsesToLoot;

        public static List<string> IgnoreItems = new List<string>();

        public string LootingRulesLocation = $"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\LootBuddy\\LootingRules.json";
        //public string ConfigLocation = $"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\LootBuddy\\{Game.ClientInst}\\Config.json";

        public static bool Start = true;

        public static string PluginDirectory;

        //public static IPCChannel IPCChannel { get; private set; }

        //public static Config Config { get; private set; }

        public override void Run(string pluginDir)
        {
            try
            {
                PluginDirectory = pluginDir;

                //Config = Config.Load(ConfigLocation);
                LootingRules.Load(LootingRulesLocation);

                //IPCChannel = new IPCChannel(Convert.ToByte(Config.IPCChannel));

                Chat.WriteLine("LootBuddy loaded!");
                Chat.WriteLine("/lootbuddy for settings.");

                LootBuddySettings.AddVariable("Running", false);
                LootBuddySettings.AddVariable("ApplyRules", false);
                LootBuddySettings.AddVariable("SingleItem", false);

                LootBuddySettings["Running"] = false;

                //Chat.RegisterCommand("buddy", LootBuddyCommand);

                SettingsController.RegisterSettingsWindow("LootBuddy", pluginDir + "\\UI\\LootBuddySettingWindow.xml", LootBuddySettings);

                Game.OnUpdate += OnUpdate;
                Inventory.ContainerOpened = OnContainerOpened;
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        private bool ItemExists(Item item)
        {
            if (extrabag1 != null && !extrabag1.Items.Contains(item) || (extrabag2 != null && !extrabag2.Items.Contains(item))
                 || (extrabag3 != null && !extrabag3.Items.Contains(item)) || (extrabag4 != null && !extrabag4.Items.Contains(item))
                 || !Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Contains(item))
                return false;
            else
                return true;
        }

        private void OnContainerOpened(object sender, Container container)
        {
            if (!LootBuddySettings["Running"].AsBool()) { return; }

            if (container.Identity.Type == IdentityType.Corpse && container.Items.Count >= 0)
            {
                foreach (Item item in container.Items)
                {
                    if (LootBuddySettings["ApplyRules"].AsBool())
                    {
                        if (LootingRules.Apply(item))
                        {
                            if (LootBuddySettings["SingleItem"].AsBool() && !ItemExists(item))
                                continue;

                            if (Inventory.NumFreeSlots >= 1)
                                item.MoveToInventory();
                            else
                            {
                                if (extrabag1 != null && extrabag1.Items.Count < 21)
                                {
                                    foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                    {
                                        if (LootingRules.Apply(itemtomove))
                                            itemtomove.MoveToContainer(extrabag1);
                                    }
                                    item.MoveToInventory();
                                }
                                else if (extrabag2 != null && extrabag2.Items.Count < 21)
                                {
                                    foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                    {
                                        if (LootingRules.Apply(itemtomove))
                                            itemtomove.MoveToContainer(extrabag2);
                                    }
                                    item.MoveToInventory();
                                }
                                else if (extrabag3 != null && extrabag3.Items.Count < 21)
                                {
                                    foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                    {
                                        if (LootingRules.Apply(itemtomove))
                                            itemtomove.MoveToContainer(extrabag3);
                                    }
                                    item.MoveToInventory();
                                }
                                else if (extrabag4 != null && extrabag4.Items.Count < 21)
                                {
                                    foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                    {
                                        if (LootingRules.Apply(itemtomove))
                                            itemtomove.MoveToContainer(extrabag4);
                                    }
                                    item.MoveToInventory();
                                }
                                else if (extrabag5 != null && extrabag5.Items.Count < 21)
                                {
                                    foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                    {
                                        if (LootingRules.Apply(itemtomove))
                                            itemtomove.MoveToContainer(extrabag5);
                                    }
                                    item.MoveToInventory();
                                }
                                else if (extrabag6 != null && extrabag6.Items.Count < 21)
                                {
                                    foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                    {
                                        if (LootingRules.Apply(itemtomove))
                                            itemtomove.MoveToContainer(extrabag6);
                                    }
                                    item.MoveToInventory();
                                }
                                else if (extrabag7 != null && extrabag7.Items.Count < 21)
                                {
                                    foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                    {
                                        if (LootingRules.Apply(itemtomove))
                                            itemtomove.MoveToContainer(extrabag7);
                                    }
                                    item.MoveToInventory();
                                }
                                else if (extrabag8 != null && extrabag8.Items.Count < 21)
                                {
                                    foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                    {
                                        if (LootingRules.Apply(itemtomove))
                                            itemtomove.MoveToContainer(extrabag8);
                                    }
                                    item.MoveToInventory();
                                }
                                else if (extrabag9 != null && extrabag9.Items.Count < 21)
                                {
                                    foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                    {
                                        if (LootingRules.Apply(itemtomove))
                                            itemtomove.MoveToContainer(extrabag9);
                                    }
                                    item.MoveToInventory();
                                }
                                else if (extrabag10 != null && extrabag10.Items.Count < 21)
                                {
                                    foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                    {
                                        if (LootingRules.Apply(itemtomove))
                                            itemtomove.MoveToContainer(extrabag10);
                                    }
                                    item.MoveToInventory();
                                }
                                else if (extrabag11 != null && extrabag11.Items.Count < 21)
                                {
                                    foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                    {
                                        if (LootingRules.Apply(itemtomove))
                                            itemtomove.MoveToContainer(extrabag11);
                                    }
                                    item.MoveToInventory();
                                }
                                else if (extrabag12 != null && extrabag12.Items.Count < 21)
                                {
                                    foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                    {
                                        if (LootingRules.Apply(itemtomove))
                                            itemtomove.MoveToContainer(extrabag12);
                                    }
                                    item.MoveToInventory();
                                }
                                else if (extrabag13 != null && extrabag13.Items.Count < 21)
                                {
                                    foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                    {
                                        if (LootingRules.Apply(itemtomove))
                                            itemtomove.MoveToContainer(extrabag13);
                                    }
                                    item.MoveToInventory();
                                }
                                else if (extrabag14 != null && extrabag14.Items.Count < 21)
                                {
                                    foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                    {
                                        if (LootingRules.Apply(itemtomove))
                                            itemtomove.MoveToContainer(extrabag14);
                                    }
                                    item.MoveToInventory();
                                }
                                else if (extrabag15 != null && extrabag15.Items.Count < 21)
                                {
                                    foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                    {
                                        if (LootingRules.Apply(itemtomove))
                                            itemtomove.MoveToContainer(extrabag15);
                                    }
                                    item.MoveToInventory();
                                }
                                else if (extrabag16 != null && extrabag16.Items.Count < 21)
                                {
                                    foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                    {
                                        if (LootingRules.Apply(itemtomove))
                                            itemtomove.MoveToContainer(extrabag16);
                                    }
                                    item.MoveToInventory();
                                }
                                else if (extrabag17 != null && extrabag17.Items.Count < 21)
                                {
                                    foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                    {
                                        if (LootingRules.Apply(itemtomove))
                                            itemtomove.MoveToContainer(extrabag17);
                                    }
                                    item.MoveToInventory();
                                }
                                else if (extrabag18 != null && extrabag18.Items.Count < 21)
                                {
                                    foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                    {
                                        if (LootingRules.Apply(itemtomove))
                                            itemtomove.MoveToContainer(extrabag18);
                                    }
                                    item.MoveToInventory();
                                }
                            }
                        }
                        else
                        {
                            item.Delete();
                        }
                    }
                    else
                    {
                        if (Inventory.NumFreeSlots >= 1)
                            item.MoveToInventory();
                        else
                        {
                            if (extrabag1 != null && extrabag1.Items.Count < 21)
                            {
                                foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                {
                                    if (itemtomove.Name != "Health and Nano Recharger" && itemtomove.Name != "Health and Nano Stim"
                                        && itemtomove.Name != "Aggression Enhancer")
                                        itemtomove.MoveToContainer(extrabag1);
                                }
                                item.MoveToInventory();
                            }
                            else if (extrabag2 != null && extrabag2.Items.Count < 21)
                            {
                                foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                {
                                    if (itemtomove.Name != "Health and Nano Recharger" && itemtomove.Name != "Health and Nano Stim"
                                        && itemtomove.Name != "Aggression Enhancer")
                                        itemtomove.MoveToContainer(extrabag2);
                                }
                                item.MoveToInventory();
                            }
                            else if (extrabag3 != null && extrabag3.Items.Count < 21)
                            {
                                foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                {
                                    if (itemtomove.Name != "Health and Nano Recharger" && itemtomove.Name != "Health and Nano Stim"
                                        && itemtomove.Name != "Aggression Enhancer")
                                        itemtomove.MoveToContainer(extrabag3);
                                }
                                item.MoveToInventory();
                            }
                            else if (extrabag4 != null && extrabag4.Items.Count < 21)
                            {
                                foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                {
                                    if (itemtomove.Name != "Health and Nano Recharger" && itemtomove.Name != "Health and Nano Stim"
                                        && itemtomove.Name != "Aggression Enhancer")
                                        itemtomove.MoveToContainer(extrabag4);
                                }
                                item.MoveToInventory();
                            }
                            else if (extrabag5 != null && extrabag5.Items.Count < 21)
                            {
                                foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                {
                                    if (itemtomove.Name != "Health and Nano Recharger" && itemtomove.Name != "Health and Nano Stim"
                                        && itemtomove.Name != "Aggression Enhancer")
                                        itemtomove.MoveToContainer(extrabag5);
                                }
                                item.MoveToInventory();
                            }
                            else if (extrabag6 != null && extrabag6.Items.Count < 21)
                            {
                                foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                {
                                    if (itemtomove.Name != "Health and Nano Recharger" && itemtomove.Name != "Health and Nano Stim"
                                        && itemtomove.Name != "Aggression Enhancer")
                                        itemtomove.MoveToContainer(extrabag6);
                                }
                                item.MoveToInventory();
                            }
                            else if (extrabag7 != null && extrabag7.Items.Count < 21)
                            {
                                foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                {
                                    if (itemtomove.Name != "Health and Nano Recharger" && itemtomove.Name != "Health and Nano Stim"
                                        && itemtomove.Name != "Aggression Enhancer")
                                        itemtomove.MoveToContainer(extrabag7);
                                }
                                item.MoveToInventory();
                            }
                            else if (extrabag8 != null && extrabag8.Items.Count < 21)
                            {
                                foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                {
                                    if (itemtomove.Name != "Health and Nano Recharger" && itemtomove.Name != "Health and Nano Stim"
                                        && itemtomove.Name != "Aggression Enhancer")
                                        itemtomove.MoveToContainer(extrabag8);
                                }
                                item.MoveToInventory();
                            }
                            else if (extrabag9 != null && extrabag9.Items.Count < 21)
                            {
                                foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                {
                                    if (itemtomove.Name != "Health and Nano Recharger" && itemtomove.Name != "Health and Nano Stim"
                                        && itemtomove.Name != "Aggression Enhancer")
                                        itemtomove.MoveToContainer(extrabag9);
                                }
                                item.MoveToInventory();
                            }
                            else if (extrabag10 != null && extrabag10.Items.Count < 21)
                            {
                                foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                {
                                    if (itemtomove.Name != "Health and Nano Recharger" && itemtomove.Name != "Health and Nano Stim"
                                        && itemtomove.Name != "Aggression Enhancer")
                                        itemtomove.MoveToContainer(extrabag10);
                                }
                                item.MoveToInventory();
                            }
                            else if (extrabag11 != null && extrabag11.Items.Count < 21)
                            {
                                foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                {
                                    if (itemtomove.Name != "Health and Nano Recharger" && itemtomove.Name != "Health and Nano Stim"
                                        && itemtomove.Name != "Aggression Enhancer")
                                        itemtomove.MoveToContainer(extrabag11);
                                }
                                item.MoveToInventory();
                            }
                            else if (extrabag12 != null && extrabag12.Items.Count < 21)
                            {
                                foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                {
                                    if (itemtomove.Name != "Health and Nano Recharger" && itemtomove.Name != "Health and Nano Stim"
                                        && itemtomove.Name != "Aggression Enhancer")
                                        itemtomove.MoveToContainer(extrabag12);
                                }
                                item.MoveToInventory();
                            }
                            else if (extrabag13 != null && extrabag13.Items.Count < 21)
                            {
                                foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                {
                                    if (itemtomove.Name != "Health and Nano Recharger" && itemtomove.Name != "Health and Nano Stim"
                                        && itemtomove.Name != "Aggression Enhancer")
                                        itemtomove.MoveToContainer(extrabag13);
                                }
                                item.MoveToInventory();
                            }
                            else if (extrabag14 != null && extrabag14.Items.Count < 21)
                            {
                                foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                {
                                    if (itemtomove.Name != "Health and Nano Recharger" && itemtomove.Name != "Health and Nano Stim"
                                        && itemtomove.Name != "Aggression Enhancer")
                                        itemtomove.MoveToContainer(extrabag14);
                                }
                                item.MoveToInventory();
                            }
                            else if (extrabag15 != null && extrabag15.Items.Count < 21)
                            {
                                foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                {
                                    if (itemtomove.Name != "Health and Nano Recharger" && itemtomove.Name != "Health and Nano Stim"
                                        && itemtomove.Name != "Aggression Enhancer")
                                        itemtomove.MoveToContainer(extrabag15);
                                }
                                item.MoveToInventory();
                            }
                            else if (extrabag16 != null && extrabag16.Items.Count < 21)
                            {
                                foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                {
                                    if (itemtomove.Name != "Health and Nano Recharger" && itemtomove.Name != "Health and Nano Stim"
                                        && itemtomove.Name != "Aggression Enhancer")
                                        itemtomove.MoveToContainer(extrabag16);
                                }
                                item.MoveToInventory();
                            }
                            else if (extrabag17 != null && extrabag17.Items.Count < 21)
                            {
                                foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                {
                                    if (itemtomove.Name != "Health and Nano Recharger" && itemtomove.Name != "Health and Nano Stim"
                                        && itemtomove.Name != "Aggression Enhancer")
                                        itemtomove.MoveToContainer(extrabag17);
                                }
                                item.MoveToInventory();
                            }
                            else if (extrabag18 != null && extrabag18.Items.Count < 21)
                            {
                                foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
                                {
                                    if (itemtomove.Name != "Health and Nano Recharger" && itemtomove.Name != "Health and Nano Stim"
                                        && itemtomove.Name != "Aggression Enhancer")
                                        itemtomove.MoveToContainer(extrabag18);
                                }
                                item.MoveToInventory();
                            }
                        }
                    }
                }

                //if (corpseLootingIdentity.Count >= 1)
                //{
                //    corpseLooting = DynelManager.Corpses
                //        .Where(corpse => corpseLootingIdentity.Contains(corpse.Identity))
                //        .Where(corpse => corpse.DistanceFrom(DynelManager.LocalPlayer) < 5)
                //        .ToList();

                //    corpseLooting.FirstOrDefault().Open();
                //    corpseLootingIdentity.Remove(corpseLooting.FirstOrDefault().Identity);
                //}
            }
        }

        private void AddIgnoreItem(object s, ButtonBase button)
        {
            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                SettingsController.settingsWindow.FindView("IgnoreItem", out TextInputView textinput1);

                if (textinput1 != null && textinput1.Text != String.Empty)
                {
                    Task.Factory.StartNew(
                     async () =>
                     {
                         await Task.Delay(200);

                         IgnoreItems.Add(textinput1.Text);
                         Chat.WriteLine($"Ignore item {textinput1.Text} added.");
                     });
                }
            }
        }

        private void ClearIgnoreList(object s, ButtonBase button)
        {
            Chat.WriteLine($"List cleared.");
            IgnoreItems.Clear();
        }

        private void PrintIgnoreList(object s, ButtonBase button)
        {
            if (IgnoreItems.Count() == 0)
                Chat.WriteLine($"List empty.");

            foreach (string ignoreItem in IgnoreItems)
            {
                Chat.WriteLine($"{ignoreItem}");
            }
        }

        private void AddRules(object s, ButtonBase button)
        {
            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                SettingsController.settingsWindow.FindView("QlMinBox", out TextInputView textinput1);
                SettingsController.settingsWindow.FindView("QlMaxBox", out TextInputView textinput2);
                SettingsController.settingsWindow.FindView("ItemNameBox", out TextInputView textinput3);

                if (textinput1 != null && textinput1.Text != String.Empty && int.TryParse(textinput1.Text, out int minQl)
                    && textinput2 != null && textinput2.Text != String.Empty && int.TryParse(textinput2.Text, out int maxQl)
                    && textinput3 != null && textinput3.Text != String.Empty)
                {
                    Task.Factory.StartNew(
                     async () =>
                     {
                         await Task.Delay(200);

                         LootingRules.Add(minQl, maxQl, textinput3.Text);
                         Chat.WriteLine($"Rule added.");
                     });
                }
            }
        }

        private void ClearRules(object s, ButtonBase button)
        {
            Chat.WriteLine($"List cleared.");
            LootingRules.Clear();
        }

        private void PrintRules(object s, ButtonBase button)
        {
            LootingRules.DumpToChat();
        }

        private void HelpBox(object s, ButtonBase button)
        {
            Window helpWindow = Window.CreateFromXml("Help", PluginDirectory + "\\UI\\LootBuddyHelpBox.xml",
            windowSize: new Rect(0, 0, 455, 345),
            windowStyle: WindowStyle.Default,
            windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);
            helpWindow.Show(true);
        }

        private void OnUpdate(object sender, float deltaTime)
        {
            try
            {
                if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
                {
                    //SettingsController.settingsWindow.FindView("ChannelBox", out TextInputView textinput1);

                    //if (textinput1 != null && textinput1.Text != String.Empty)
                    //{
                    //    if (int.TryParse(textinput1.Text, out int channelValue))
                    //    {
                    //        if (Config.CharSettings[Game.ClientInst].IPCChannel != channelValue)
                    //        {
                    //            IPCChannel.SetChannelId(Convert.ToByte(channelValue));
                    //            Config.CharSettings[Game.ClientInst].IPCChannel = Convert.ToByte(channelValue);
                    //            SettingsController.LootBuddyChannel = channelValue.ToString();
                    //            Config.Save();
                    //        }
                    //    }
                    //}

                    if (SettingsController.settingsView != null)
                    {
                        if (SettingsController.settingsView.FindChild("LootBuddyHelpBox", out Button helpBox))
                        {
                            helpBox.Tag = SettingsController.settingsView;
                            helpBox.Clicked = HelpBox;
                        }

                        if (SettingsController.settingsView.FindChild("AddList", out Button addBox))
                        {
                            addBox.Tag = SettingsController.settingsView;
                            addBox.Clicked = AddRules;
                        }

                        if (SettingsController.settingsView.FindChild("PrintList", out Button printBox))
                        {
                            printBox.Tag = SettingsController.settingsView;
                            printBox.Clicked = PrintRules;
                        }

                        if (SettingsController.settingsView.FindChild("ClearList", out Button clearBox))
                        {
                            clearBox.Tag = SettingsController.settingsView;
                            clearBox.Clicked = ClearRules;
                        }

                        if (SettingsController.settingsView.FindChild("AddIgnoreItem", out Button addIgnoreBox))
                        {
                            addIgnoreBox.Tag = SettingsController.settingsView;
                            addIgnoreBox.Clicked = AddIgnoreItem;
                        }

                        if (SettingsController.settingsView.FindChild("PrintIgnoreList", out Button printIgnoreBox))
                        {
                            printIgnoreBox.Tag = SettingsController.settingsView;
                            printIgnoreBox.Clicked = PrintIgnoreList;
                        }

                        if (SettingsController.settingsView.FindChild("ClearIgnoreList", out Button clearIgnoreBox))
                        {
                            clearIgnoreBox.Tag = SettingsController.settingsView;
                            clearIgnoreBox.Clicked = ClearIgnoreList;
                        }
                    }
                }


                //if (SettingsController.LootBuddyChannel == String.Empty)
                //{
                //    SettingsController.LootBuddyChannel = Config.IPCChannel.ToString();
                //}

                if (LootBuddySettings["Running"].AsBool())
                {
                    if (Time.NormalTime - _lastCheckTime > new Random().Next(1, 6))
                    {
                        _lastCheckTime = Time.NormalTime;

                        corpsesToLoot = DynelManager.Corpses
                            .Where(corpse => corpse.DistanceFrom(DynelManager.LocalPlayer) < 7)
                            .FirstOrDefault();

                        if (corpsesToLoot != null)
                        {
                            corpsesToLoot.Open();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Chat.WriteLine(e);
            }
        }

        //private void LootBuddyCommand(string command, string[] param, ChatWindow chatWindow)
        //{
        //    try
        //    {
        //        if (param.Length < 1)
        //        {
        //            if (!LootBuddySettings["Running"].AsBool())
        //            {
        //                LootBuddySettings["Running"] = true;
        //                Chat.WriteLine("Bot enabled.");
        //            }
        //            else
        //            {
        //                Chat.WriteLine("Bot disabled.");
        //                LootBuddySettings["Running"] = false;
        //            }
        //            return;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Chat.WriteLine(e.Message);
        //    }
        //}

        public override void Teardown()
        {
            LootingRules.Save();
        }
    }
}
